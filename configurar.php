<?php 

	require 'app/db.php';

	if ( isset($_GET['id']) ) $id = $_GET['id'];
	if ( isset($_GET['folder-new']) ) $folder = $_GET['folder-new'];
	if ( isset($_GET['url-new']) ) $url = $_GET['url-new'];
	if ( isset($_GET['toggle']) ) $toggle = $_GET['toggle'];
	if ( isset($_GET['delete']) ) $delete = $_GET['delete'];

	// verifica se existe id
	if ( isset($_GET['id']) ) {

		// ativar projeto
		if ( isset($toggle) ) {
			update( 'host', 'status', 1, $id );
		}

		// desativar projeto
		if ( !isset($toggle) ) {
			update( 'host', 'status', 2, $id );
		}

		// alterar folder
		if ( isset($folder) ) {
			update( 'host', 'folder', $folder, $id );
		}

		// alterar url
		if ( isset($url) ) {
			update( 'host', 'url', $url, $id );
		}

		// deletar host
		if ( isset($delete) && $delete == 'yes' ) {
			delete( 'host', $id );
		}

		// roda o bat
		cadastrarHost();

		// redireciona para inicio
		header("location: index.php");

	}