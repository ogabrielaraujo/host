<?php 
	
	include 'app/db.php';

	if ( !isset($_GET['folder']) && !isset($_GET['url']) ) {
		echo "Algo esta errado :/";
	} else {
		// vars
		$folder = $_GET['folder'];
		$url = $_GET['url'];

		// Verificar se host já existe, se não existir, continua o script
		verificarHost( $url );

		// Adicionar no banco
		$host = array(
			'folder' 	=> $folder,
			'url' 		=> $url,
			'status'	=> 1
		);
		$cadastrar = insert( 'host', $host );

		if ( $cadastrar == false ) {
			echo "algo esta errado :/";
		}

		cadastrarHost();

		// redirecionar
		header("location: index.php");
	}