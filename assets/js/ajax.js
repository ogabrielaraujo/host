jQuery(document).ready(function($) {

	// mostrar/esconder formulários
	$('.add').click(function() {
		$('.add').toggleClass('active');
		$('.adicionar').toggleClass('hide');
		$('.configurar').toggleClass('hide');
	});

	var lista = $('.lista');
	var toggle = $('#switch-1');

	lista.on('change', function() {
		// pegando id do projeto selecionado
		var id = $('.lista option:selected').attr('name');

		// mostrando/escondendo opções caso select vazio
		if ( id != 'Selecione' ) {
			$('.folder-new').removeClass('hide');
			$('.url-new').removeClass('hide');
			$('.sub').removeClass('hide');
			$('.switch').removeClass('hide');
			$('.options').removeClass('hide');
		} else {
			$('.folder-new').addClass('hide');
			$('.url-new').addClass('hide');
			$('.sub').addClass('hide');
			$('.switch').addClass('hide');
			$('.options').addClass('hide');
		}
		
		// atribuindo valor do id ao input extra (escondido)
		$('.projetoid').attr('value', id);

		$.ajax({
			url: 'status.php',
			type: 'GET',
			data: $('.form').serialize(),
			dataType: 'json',
			success: function (data) {
				// pegando valores dos options
				var statusAtual = $('.lista option:selected').attr('status');
				var folderAtual = $('.lista option:selected').attr('folder');
				var urlAtual 	= $('.lista option:selected').attr('url');

				// preenchendo toggle com valor do banco de acordo com option selecionado
				if ( statusAtual == 1 ) {
					toggle.prop('checked', true);
				}

				if ( statusAtual == 2 ) {
					toggle.prop('checked', false);
				}

				// preenchendo input com valor do banco de acordo com option selecionado
				$('.folder-new').val(folderAtual);
				$('.url-new').val(urlAtual);
			}
		});
	});

	// adicionar input para passar $_GET['delete']
	$('.remover').on('click', function() {
		var r = confirm('Tem certeza?');
		if ( r == true ) {
			$('.remover').append('<input type="text" class="hide" name="delete" value="yes" />');
		}
	});

});