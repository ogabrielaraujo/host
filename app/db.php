<?php 

	// banco.sqlite
	try {
		// connect
		$db = new PDO('sqlite:app/banco.sqlite');
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		// table host
		$tb_host = "
			CREATE TABLE IF NOT EXISTS host
			( 
				id INTEGER PRIMARY KEY AUTOINCREMENT, 
				folder VARCHAR(255) NOT NULL,
				url VARCHAR(255) NOT NULL,
				status INT NOT NULL
			)
		";
		$db->exec($tb_host);
	} catch ( PDOException $e ) {
		echo $e->getMessage();
		exit();
	}

	// dados
	try {
		function insert( $tabela, array $dados ) {
			global $db;

			// reading array dados
			$campos = implode(", ", array_keys($dados));
			$values = ":" . implode( ", :", array_keys($dados) );
			$keys = array_keys($dados);
			$val = array_values($dados);

			// query
			$query = "INSERT INTO {$tabela} ({$campos}) VALUES ({$values})";

			// prepare
			$stmt = $db->prepare($query);

			// bind values
			for ($i=0; $i<count($dados); $i++) {
				$stmt->bindValue( ":" . $keys[$i], $val[$i] );
			}

			// execute
			$stmt->execute();
			
			// result
			if ( $stmt->rowCount() >= 1 ) {
				return true;
			} else {
				return false;
			}
		}

		function select( $tabela, $coluna = null, $valor = null ) {
			global $db;

			// query
			if ( $coluna == null && $valor == null ) {
				$query = "SELECT * FROM {$tabela}";
			} else {
				// verificando tipo de valor
				if ( is_int($valor) ) {
					$query = "SELECT * FROM {$tabela} WHERE $coluna = $valor";
				} else {
					$query = "SELECT * FROM {$tabela} WHERE $coluna = '$valor'";
				}
			}

			// execute
			$stmt = $db->prepare($query);
			$stmt->execute();

			// results
			/*while ( $linha = $rs->fetch(PDO::FETCH_OBJ) ) {
				echo "<pre>";
				print_r($linha);
				echo "</pre>";
			}*/

			return $rs = $stmt->fetchAll(PDO::FETCH_OBJ);
		}

		function num_row( $tabela ) {
			global $db;

			// query
			$query = "SELECT count(*) FROM {$tabela}";

			// execute
			$stmt = $db->prepare($query);
			$stmt->execute();

			// count
			$count = $stmt->fetchColumn();

			return $count;
		}

		function update( $tabela, $coluna, $novoValor, $id ) {
			global $db;

			// query
			$query = "UPDATE {$tabela} SET {$coluna} = '{$novoValor}' WHERE id = {$id}";

			// execute
			$stmt = $db->prepare($query);
			$stmt->execute();
		}

		function delete( $tabela, $id ) {
			global $db;

			// query
			$query = "DELETE FROM {$tabela} WHERE id = {$id}";

			// execute
			$stmt = $db->prepare($query);
			$stmt->execute();
		}

		function cadastrarHost() {
			// arquivo bat
			$file = "host.bat";

			// pegando valores cadastrados
			$data = select( 'host' );

			// hosts
			$content = 
				"cd C:/windows/system32/drivers/etc" . PHP_EOL . 
				"echo 127.0.0.1 		localhost > hosts" . PHP_EOL;

				foreach ($data as $key => $value) {
					// verificar se esta ativo
					if ( $value->status == "2" ) {
						continue;
					}

					$content .= "echo 127.0.0.1 		" . $value->url . " >> hosts" . PHP_EOL;
				}

			// httpd-vhosts.conf
			$content .= PHP_EOL . 
				"cd C:/xampp/apache/conf/extra" . PHP_EOL . 
				"echo # Virtual Hosts > httpd-vhosts.conf" . PHP_EOL . 
				"echo NameVirtualHost *:80 >> httpd-vhosts.conf" . PHP_EOL . 
				"echo. >> httpd-vhosts.conf" . PHP_EOL . 
				"echo 	# localhost >> httpd-vhosts.conf" . PHP_EOL . 
				"echo 	^<VirtualHost *:80^> >> httpd-vhosts.conf" . PHP_EOL . 
				"echo 			DocumentRoot 'C:/xampp/htdocs' >> httpd-vhosts.conf" . PHP_EOL . 
				"echo 			ServerName localhost >> httpd-vhosts.conf" . PHP_EOL . 
				"echo 			^<Directory 'C:/xampp/htdocs'^> >> httpd-vhosts.conf" . PHP_EOL . 
				"echo 				Require all granted >> httpd-vhosts.conf" . PHP_EOL . 
				"echo 			^</Directory^> >> httpd-vhosts.conf" . PHP_EOL . 
				"echo 	^</VirtualHost^> >> httpd-vhosts.conf" . PHP_EOL . 
				"echo. >> httpd-vhosts.conf" . PHP_EOL . PHP_EOL . PHP_EOL . 
				"echo # CUSTOM >> httpd-vhosts.conf" . PHP_EOL . 
				"echo. >> httpd-vhosts.conf" . PHP_EOL . PHP_EOL
			;

				// adicionar cada host cadastrado no arquivo bat
				foreach ($data as $key => $value) {
				
					// verificar se esta ativo
					if ( $value->status == "2" ) {
						continue;
					}

					$content .= 
						"echo 	# " . $value->url . " >> httpd-vhosts.conf" . PHP_EOL . 
						"echo 	^<VirtualHost *:80^> >> httpd-vhosts.conf" . PHP_EOL . 
						"echo 			DocumentRoot '" . $value->folder . "' >> httpd-vhosts.conf" . PHP_EOL . 
						"echo 			ServerName " . $value->url . " >> httpd-vhosts.conf" . PHP_EOL . 
						"echo 			^<Directory '" . $value->folder . "'^> >> httpd-vhosts.conf" . PHP_EOL . 
						"echo 				Require all granted >> httpd-vhosts.conf" . PHP_EOL . 
						"echo 			^</Directory^> >> httpd-vhosts.conf" . PHP_EOL . 
						"echo 	^</VirtualHost^> >> httpd-vhosts.conf" . PHP_EOL . 
						"echo. >> httpd-vhosts.conf" . PHP_EOL . PHP_EOL
					;
				}

			// Adiciona conteúdo ao arquivo bat
			file_put_contents($file, $content);

			exec($file);
		}

		function verificarHost( $url ) {
			$verificar = select( 'host', 'url', $url );

			$count = count($verificar);

			// se existir dados
			if ( $count > 0 ) {
				echo 'Host existente, tente criar um host com um nome e url diferentes.';
				exit();
			}
		}

	} catch ( PDOException $e ) {
		echo $e->getMessage();
		exit();
	}
