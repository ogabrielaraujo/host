<?php require 'app/db.php'; ?>
<!DOCTYPE html>
<html lang="pt-Br">
<head>
	<title>Host</title>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" type="image/png" href="dist/img/favicon.png" />
	<link rel="stylesheet" href="dist/css/style.min.css" />
</head>
<body class="no-select home">

	<div class="wrap">
		<div class="box">
			<!-- Botão lateral -->
			<button class="round blue add">+</button>

			<!-- Configurar -->
			<div class="configurar">
				<span class="title">host</span>

				<form method="GET" action="configurar.php" class="form config">
					<span class="select">
						<select name="" class="lista">
							<?php 
								$list = select( 'host' );

								echo "<option name='Selecione' status='2'>Selecione</option>";

								foreach ($list as $key => $value) {
									echo "<option name='$value->id' status='$value->status' url='$value->url' folder='$value->folder'>
											$value->url
										</option>";
								}
							?>
						</select>
					</span>

					<input type="text" class="folder-new hide" name="folder-new" placeholder="C:/projeto" autocomplete="off" />
					<span class="sub hide">Pasta</span>

					<input type="text" class="url-new hide" name="url-new" placeholder="projeto.local.com" autocomplete="off" />
					<span class="sub hide">Url</span>

					<input type="text" class="projetoid hide" value="" name="id" />
				
					<div class="switch hide">
						<input name="toggle" id="switch-1" class="toggle" type="checkbox" />
						<label for="switch-1"></label>
						<div class="tooltip">Status On/Off</div>
					</div>

					<div class="options hide">
						<button class="btn round red remover">
							<i class="icon-trash"></i>
							<div class="tooltip">Excluir</div>
						</button>
						
						<button type="submit" class="btn round blue atualizar">
							<i class="icon-paper-plane"></i>
							<div class="tooltip">Atualizar</div>
						</button>
					</div>
				</form>
			</div>

			<!-- Adicionar -->
			<div class="adicionar hide">
				<span class="title">adicionar</span>

				<form method="GET" action="cadastrar.php" class="form">
					<input type="text" class="folder" name="folder" placeholder="C:\projeto" autocomplete="off" />
					<span class="sub">Pasta do projeto</span>
				
					<input type="text" class="url" name="url" placeholder="projeto.local.com" autocomplete="off" />
					<span class="sub">Url desejada para o projeto</span>
				
					<button type="submit" class="btn blue cadastrar">Cadastrar</button>
				
					<a href="http://host.local.com" target="_blank" class="ok hide">
						<input class="btn" type="button" value="Criado com sucesso !" />
					</a>
				</form>
			</div>
			
		</div>
	</div>

	<script src="dist/js/script.js"></script>
</body>
</html>