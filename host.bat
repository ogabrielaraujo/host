cd C:/windows/system32/drivers/etc
echo 127.0.0.1 		localhost > hosts
echo 127.0.0.1 		gabrielquefez.local.com >> hosts
echo 127.0.0.1 		host.local.com >> hosts
echo 127.0.0.1 		task.local.com >> hosts
echo 127.0.0.1 		framework.local.com >> hosts
echo 127.0.0.1 		composer.local.com >> hosts
echo 127.0.0.1 		wordlite.local.com >> hosts
echo 127.0.0.1 		taskpro.local.com >> hosts
echo 127.0.0.1 		somewhere.local.com >> hosts

cd C:/xampp/apache/conf/extra
echo # Virtual Hosts > httpd-vhosts.conf
echo NameVirtualHost *:80 >> httpd-vhosts.conf
echo. >> httpd-vhosts.conf
echo 	# localhost >> httpd-vhosts.conf
echo 	^<VirtualHost *:80^> >> httpd-vhosts.conf
echo 			DocumentRoot 'C:/xampp/htdocs' >> httpd-vhosts.conf
echo 			ServerName localhost >> httpd-vhosts.conf
echo 			^<Directory 'C:/xampp/htdocs'^> >> httpd-vhosts.conf
echo 				Require all granted >> httpd-vhosts.conf
echo 			^</Directory^> >> httpd-vhosts.conf
echo 	^</VirtualHost^> >> httpd-vhosts.conf
echo. >> httpd-vhosts.conf


echo # CUSTOM >> httpd-vhosts.conf
echo. >> httpd-vhosts.conf

echo 	# gabrielquefez.local.com >> httpd-vhosts.conf
echo 	^<VirtualHost *:80^> >> httpd-vhosts.conf
echo 			DocumentRoot 'C:\xampp\htdocs\projects\gabrielquefez' >> httpd-vhosts.conf
echo 			ServerName gabrielquefez.local.com >> httpd-vhosts.conf
echo 			^<Directory 'C:\xampp\htdocs\projects\gabrielquefez'^> >> httpd-vhosts.conf
echo 				Require all granted >> httpd-vhosts.conf
echo 			^</Directory^> >> httpd-vhosts.conf
echo 	^</VirtualHost^> >> httpd-vhosts.conf
echo. >> httpd-vhosts.conf

echo 	# host.local.com >> httpd-vhosts.conf
echo 	^<VirtualHost *:80^> >> httpd-vhosts.conf
echo 			DocumentRoot 'C:\dev\host' >> httpd-vhosts.conf
echo 			ServerName host.local.com >> httpd-vhosts.conf
echo 			^<Directory 'C:\dev\host'^> >> httpd-vhosts.conf
echo 				Require all granted >> httpd-vhosts.conf
echo 			^</Directory^> >> httpd-vhosts.conf
echo 	^</VirtualHost^> >> httpd-vhosts.conf
echo. >> httpd-vhosts.conf

echo 	# task.local.com >> httpd-vhosts.conf
echo 	^<VirtualHost *:80^> >> httpd-vhosts.conf
echo 			DocumentRoot 'C:\xampp\htdocs\projects\task' >> httpd-vhosts.conf
echo 			ServerName task.local.com >> httpd-vhosts.conf
echo 			^<Directory 'C:\xampp\htdocs\projects\task'^> >> httpd-vhosts.conf
echo 				Require all granted >> httpd-vhosts.conf
echo 			^</Directory^> >> httpd-vhosts.conf
echo 	^</VirtualHost^> >> httpd-vhosts.conf
echo. >> httpd-vhosts.conf

echo 	# framework.local.com >> httpd-vhosts.conf
echo 	^<VirtualHost *:80^> >> httpd-vhosts.conf
echo 			DocumentRoot 'C:\xampp\htdocs\projects\framework' >> httpd-vhosts.conf
echo 			ServerName framework.local.com >> httpd-vhosts.conf
echo 			^<Directory 'C:\xampp\htdocs\projects\framework'^> >> httpd-vhosts.conf
echo 				Require all granted >> httpd-vhosts.conf
echo 			^</Directory^> >> httpd-vhosts.conf
echo 	^</VirtualHost^> >> httpd-vhosts.conf
echo. >> httpd-vhosts.conf

echo 	# composer.local.com >> httpd-vhosts.conf
echo 	^<VirtualHost *:80^> >> httpd-vhosts.conf
echo 			DocumentRoot 'C:\xampp\htdocs\projects\composer' >> httpd-vhosts.conf
echo 			ServerName composer.local.com >> httpd-vhosts.conf
echo 			^<Directory 'C:\xampp\htdocs\projects\composer'^> >> httpd-vhosts.conf
echo 				Require all granted >> httpd-vhosts.conf
echo 			^</Directory^> >> httpd-vhosts.conf
echo 	^</VirtualHost^> >> httpd-vhosts.conf
echo. >> httpd-vhosts.conf

echo 	# wordlite.local.com >> httpd-vhosts.conf
echo 	^<VirtualHost *:80^> >> httpd-vhosts.conf
echo 			DocumentRoot 'C:\xampp\htdocs\projects\wordlite' >> httpd-vhosts.conf
echo 			ServerName wordlite.local.com >> httpd-vhosts.conf
echo 			^<Directory 'C:\xampp\htdocs\projects\wordlite'^> >> httpd-vhosts.conf
echo 				Require all granted >> httpd-vhosts.conf
echo 			^</Directory^> >> httpd-vhosts.conf
echo 	^</VirtualHost^> >> httpd-vhosts.conf
echo. >> httpd-vhosts.conf

echo 	# taskpro.local.com >> httpd-vhosts.conf
echo 	^<VirtualHost *:80^> >> httpd-vhosts.conf
echo 			DocumentRoot 'C:\xampp\htdocs\projects\taskpro' >> httpd-vhosts.conf
echo 			ServerName taskpro.local.com >> httpd-vhosts.conf
echo 			^<Directory 'C:\xampp\htdocs\projects\taskpro'^> >> httpd-vhosts.conf
echo 				Require all granted >> httpd-vhosts.conf
echo 			^</Directory^> >> httpd-vhosts.conf
echo 	^</VirtualHost^> >> httpd-vhosts.conf
echo. >> httpd-vhosts.conf

echo 	# somewhere.local.com >> httpd-vhosts.conf
echo 	^<VirtualHost *:80^> >> httpd-vhosts.conf
echo 			DocumentRoot 'C:\xampp\htdocs\projects\somewhere' >> httpd-vhosts.conf
echo 			ServerName somewhere.local.com >> httpd-vhosts.conf
echo 			^<Directory 'C:\xampp\htdocs\projects\somewhere'^> >> httpd-vhosts.conf
echo 				Require all granted >> httpd-vhosts.conf
echo 			^</Directory^> >> httpd-vhosts.conf
echo 	^</VirtualHost^> >> httpd-vhosts.conf
echo. >> httpd-vhosts.conf

